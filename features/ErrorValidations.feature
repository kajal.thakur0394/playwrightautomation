Feature: Ecommerce2 Validations

  @Validations
  Scenario Outline: Placing the Order
    Given a loging to Ecommerce2 application with "<username>" and "<password>"
    Then verify error message is displayed

    Examples: 
      | username          | password    |
      | anshika@gmail.com | Iamking@000 |
      | test@gmail.com    | Iamking@000 |
