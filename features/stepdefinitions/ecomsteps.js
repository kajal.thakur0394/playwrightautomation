const { Given, When, Then } = require('@cucumber/cucumber');
const { expect } = require('@playwright/test');
const playwright = require('@playwright/test');
const { POManager } = require("../../pageobjects/POManager");

Given('a loging to Ecommerce application with {string} and {string}', { timeout: 100 * 1000 }, async function (email, password) {
    const loginPage = this.poManager.getLoginPage();
    loginPage.goToLoginPage();
    await loginPage.validLogin(email, password);
});
When('Add {string} to cart', async function (productName) {
    this.dashboardPage = this.poManager.getDashboardPage();
    await this.dashboardPage.searchProduct(productName);
    await this.dashboardPage.navigateToCart();
});
Then('verify {string} is displayed in the cart', async function (productName) {
    const cartPage = this.poManager.getCartPage();
    await cartPage.verifyProductIsDisplayed(productName);
    await cartPage.checkOut();
});
When('enter valid details and place the Order', async function () {
    const orderReviewPage = this.poManager.getOrdersReviewPage();
    await orderReviewPage.searchCountryAndSelect("ind", " India");
    //await orderReviewPage.verifyEmailId(email);
    this.orderId = await orderReviewPage.submitAndGetOrderId();
    console.log(this.orderId);
});
Then('verify order is present in the OrderHistory', async function () {
    await this.dashboardPage.navigateToOrders();
    const ordersHistoryPage = this.poManager.getOrdersHistoryPage();
    await ordersHistoryPage.searchOrderAndSelect(this.orderId);
    const orderIdDetails = await ordersHistoryPage.getOrderId();
    expect(await this.orderId.includes(orderIdDetails)).toBeTruthy();
});
Given('a loging to Ecommerce2 application with {string} and {string}', async function (username, passWord) {
    const userName = this.page.locator("#username");
    const password = this.page.locator("[type='password']");
    const signInButton = this.page.locator("#signInBtn");

    await this.page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    // await userName.type("rahulshettyacadsemy");
    // await password.type("learning");
    await userName.type(username);
    await password.type(passWord);
    await signInButton.click();
});

Then('verify error message is displayed', async function () {
    console.log(await this.page.locator("[style*='block']").textContent());
    await expect(this.page.locator("[style*='block']")).toContainText("Incorrect username");
});
