class LoginPage {
    constructor(page) {
        this.page = page;
        this.signInBtn = page.locator("[value='Login']");
        this.userName = page.locator("#userEmail");
        this.password = page.locator("#userPassword");
    }

    async validLogin(username, password) {
        await this.userName.type(username);
        await this.password.type(password);
        await this.signInBtn.click();
        await this.page.waitForLoadState('networkidle');
    }
    async goToLoginPage() {
        await this.page.goto("https://rahulshettyacademy.com/client/");
    }
}
module.exports = { LoginPage };