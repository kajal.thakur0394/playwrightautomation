const { test, expect } = require('@playwright/test');
const{customTest}=require('../utils/test-base');
const { OrdersHistoryPage } = require('../pageobjects/OrdersHistoryPage');
const { OrdersReviewPage } = require('../pageobjects/OrdersReviewPage');
const { POManager } = require("../pageobjects/POManager");
//json->string->js object[to avoid encoding issue]
const dataSet = JSON.parse(JSON.stringify(require("../utils/placeorderTestData.json")));

test('Page Playwright test', async ({ page }) => {
    await page.goto("https://rahulshettyacademy.com/client/");
    await page.locator("#userEmail").type("anshika@gmail.com");
    await page.locator("#userPassword").type("Iamking@000");
    await page.locator("[value='Login']").click();
    await page.waitForLoadState('networkidle'); ////otherwise will return empty list coz its superfast
    console.log(await page.locator(".card-body b").allTextContents());
});
for (const data of dataSet) {
    test(` @Web Client App login for ${data.email}`, async ({ page }) => {
        const poManager = new POManager(page);

        const loginPage = poManager.getLoginPage();
        loginPage.goToLoginPage();
        await loginPage.validLogin(data.email, data.password);

        const dashboardPage = poManager.getDashboardPage();
        await dashboardPage.searchProduct(data.productName);
        await dashboardPage.navigateToCart();

        const cartPage = poManager.getCartPage();
        await cartPage.verifyProductIsDisplayed(data.productName);
        await cartPage.checkOut();

        const orderReviewPage = poManager.getOrdersReviewPage();
        await orderReviewPage.searchCountryAndSelect("ind", " India");
        await orderReviewPage.verifyEmailId(data.email);
        const orderId = await orderReviewPage.submitAndGetOrderId();
        console.log(orderId);

        await dashboardPage.navigateToOrders();
        const ordersHistoryPage = poManager.getOrdersHistoryPage();
        await ordersHistoryPage.searchOrderAndSelect(orderId);
        const orderIdDetails = await ordersHistoryPage.getOrderId();
        expect(await orderId.includes(orderIdDetails)).toBeTruthy();
    })
};
//custom fixtures
customTest('Client App login for customfixture', async ({ page,testDataForOrder }) => {
    const poManager = new POManager(page);

    const loginPage = poManager.getLoginPage();
    loginPage.goToLoginPage();
    await loginPage.validLogin(testDataForOrder.email, testDataForOrder.password);

    const dashboardPage = poManager.getDashboardPage();
    await dashboardPage.searchProduct(testDataForOrder.productName);
    await dashboardPage.navigateToCart();

    const cartPage = poManager.getCartPage();
    await cartPage.verifyProductIsDisplayed(testDataForOrder.productName);
    await cartPage.checkOut();

    const orderReviewPage = poManager.getOrdersReviewPage();
    await orderReviewPage.searchCountryAndSelect("ind", " India");
    await orderReviewPage.verifyEmailId(testDataForOrder.email);
    const orderId = await orderReviewPage.submitAndGetOrderId();
    console.log(orderId);

    await dashboardPage.navigateToOrders();
    const ordersHistoryPage = poManager.getOrdersHistoryPage();
    await ordersHistoryPage.searchOrderAndSelect(orderId);
    const orderIdDetails = await ordersHistoryPage.getOrderId();
    expect(await orderId.includes(orderIdDetails)).toBeTruthy();
});

//test files will trigger parallel
//individual tests in the file will run in sequence
