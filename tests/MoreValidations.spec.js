const { test, expect } = require('@playwright/test');

test('Pop-up validations', async ({ page }) => {
    await page.goto("https://rahulshettyacademy.com/AutomationPractice/");
    // await page.goto("http://google.com")
    // await page.goBack();
    // await page.goForward();
    await expect(page.locator("#displayed-text")).toBeVisible();
    await page.locator("#hide-textbox").click();
    await expect(page.locator("#displayed-text")).toBeHidden();
    page.on('dialog', dialog => dialog.accept());//anywhere in this page when you see a dialog event occured, go and click on accept
    await page.locator("#confirmbtn").click();
    //await page.pause();
    await page.locator("#mousehover").hover();
    const framesPage = page.frameLocator("#courses-iframe");
    framesPage.locator("li a[href*='lifetime-access']:visible").click();
    const actualText = await framesPage.locator(".text h2").textContent();
    const noSubscribers = actualText.split(" ")[1];
    console.log(noSubscribers)

});
// test.describe.configure({mode:'parallel'});
test("@Web Screenshot and visual comparison", async ({ page }) => {
    await page.goto("https://rahulshettyacademy.com/AutomationPractice/");
    await expect(page.locator("#displayed-text")).toBeVisible();
    await page.locator("#displayed-text").screenshot({ path: 'partialscreenshot.png' });
    await page.locator("#hide-textbox").click();
    await page.screenshot({
        path: 'screenshot.png'
    });
    await expect(page.locator("#displayed-text")).toBeHidden();
});
//todaycreenshot -store-> tomorrowsscreenshot->compare both and report error
test('visual testing', async ({page}) => {
    await page.goto("https://www.flightaware.com/");
    expect(await page.screenshot()).toMatchSnapshot('landing.png');
});