const { test, expect } = require('@playwright/test');

test('Browser Context Playwright test', async ({ browser }) => {
    const browserT = await browser.newContext();
    const page = await browserT.newPage();

    const userName = page.locator("#username");
    const password = page.locator("[type='password']");
    const signInButton = page.locator("#signInBtn");

    await page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    await userName.type("rahulshettyacadsemy");
    await password.type("learning");
    await signInButton.click();
    console.log(await page.locator("[style*='block']").textContent());
    await expect(page.locator("[style*='block']")).toContainText("Incorrect username");
    await userName.fill("")
    await userName.fill("rahulshettyacademy");
    // await signInButton.click();
    //line 29 will not get executed unless ,ine 21 returns true
    await Promise.all(
        [page.waitForNavigation(),
        signInButton.click(),
        ]
    );

    // console.log(await page.locator(".card-body a").first().textContent());
    // console.log(await page.locator(".card-body a").nth(1).textContent());
    console.log(await page.locator(".card-body a").allTextContents());

});

test('Page Playwright test', async ({ page }) => {
    await page.goto("https://www.youtube.com/");
    await expect(page).toHaveTitle("YouTube");
});

test('UI Controls', async ({ page }) => {
    const userName = page.locator("#username");
    const password = page.locator("[type='password']");
    const dropdown = page.locator("select.form-control")
    const documentLink = page.locator("[href*='documents-request']");

    await page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    await userName.type("rahulshettyacadsemy");
    await password.type("learning");
    await dropdown.selectOption("consult");
    await page.locator(".radiotextsty").last().click();
    await page.locator("#okayBtn").click();
    await expect(page.locator(".radiotextsty").last()).toBeChecked(); //when action is performed outside, await should be in that scope
    console.log(await page.locator(".radiotextsty").last().isChecked());
    await page.locator("#terms").click();
    await page.locator("#terms").uncheck();
    expect(await page.locator("#terms").isChecked()).toBeFalsy();
    await expect(documentLink).toHaveAttribute("class", "blinkingText");
});
//await page.pause();

test('Child windows handle', async ({ browser }) => {
    const context = await browser.newContext();
    const page = await context.newPage();

    const userName = page.locator("#username");
    const documentLink = page.locator("[href*='documents-request']");

    await page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    const [newPage] = await Promise.all(
        [context.waitForEvent('page'),
        documentLink.click(),
        ])
    console.log(await newPage.locator(".red").textContent());
    await userName.type("rahulshettyacademy");
    await page.pause()
});